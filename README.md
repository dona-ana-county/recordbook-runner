# RecordBook

RecordBook is an web based application developed at Doña Ana County to aid in the submission
and processing of Inspection of Public Record Act (IPRA) requests.

IPRA requests are to New Mexico government agencies what the Freedom of Information Act (FOIA) 
is to the US Federal Government.  This application has been designed to comply with
the requirements of the IPRA law as well as to follow a hypothetical workflow we 
follow here at Doña Ana County.  As such it should be usable at other institutions
in New Mexico if you choose to adopt our processing workflow.  Outside of New Mexico
the application may require some small modification to be in keeping with your laws.

The application is currently in alpha form and will be be seeing some significant 
further development in the near future,  as we move towards our launch.

## Credits

Much of the funding for the development of this application was provided by New
Mexico Office of the Attorney General as a part of a grant.  Doña County provided 
additional funds and significant staff time.  Most of the development work was 
performed under contract by [Spectrum Technologies](https://www.spectrumistechnology.com).

## Installation

The currently test RecordBook on RHEL 7

Requirements:
* linux
* docker ( we use docker-ce )
* docker-compose
* postgres (optional, can be run under docker or external)


### Installing docker-ce on rhel 7

https://docs.docker.com/install/linux/docker-ce/centos/

Before install:
* If you are using XFS for /var/, you need to ensure it has `ftype=1`
  > xfs_info /var | grep ftype 

  Its easier if it is created this way, but it is possible (if complicated) to change it with a liveCD:
  https://superuser.com/questions/1321926/recreating-an-xfs-file-system-with-ftype-1

Steps:
1. Enable extras repo
   > subscription-manager repos --enable rhel-7-server-extras-rpms

1. Install prereqs
    > yum install -y yum-utils device-mapper-persistent-data lvm2
1. Install Repo 
    > yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
1. Install packages
    > yum install docker-ce docker-ce-cli containerd.io
1. Configure docker:
    > cat '{"storage-driver": "overlay2"}' > /etc/docker/daemon.json
1. Enable Docker:
    > systemctl enable docker.service

    > systemctl start docker.service


### Installing docker-compose on rhel 7

https://docs.docker.com/compose/install/

https://docs.docker.com/compose/completion/

Steps as root (`sudo -s`):
1. Download script:
   > curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
1. Make executable:
   > chmod +x /usr/local/bin/docker-compose
1. Add to path:
   > ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

Optional steps:
1. Install back-completion:
   > yum install bash-completion
1. Install docker-compose bash-completion
   > curl -L https://raw.githubusercontent.com/docker/compose/1.24.1/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose


### Installing Recordbook

steps:
1. close runner repository
   > git clone https://gitlab.com/dona-ana-county/recordbook-runner.git
2. link record-book/data to location you want to store data
3. create directory recordbook-runner/data/elasticsearch
4. Change permissions of recordbook-runner/data/elasticsearch
   > chown 1000:1000 recordbook-runner/data/elasticsearch
5. Copy and edit configuration files:
   * > cp backend.env.sample backend.env
   * > nano backend.env
   * > cp common.env.sample common.env
   * > nano common.env
6. If not using external postgres
   * Edit docker-compose.yml and uncomment db stanza
     > nano docker-compose.yml
7. Customize using api-custom and frontend-custom, see readme in folders
8. Run:
   > sudo docker-compose up -d --build
